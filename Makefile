reformat:
	black --skip-string-normalization -l 119 .

install-requirements:
	pip install -r requirements.txt
