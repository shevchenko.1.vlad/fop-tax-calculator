# Fop Tax Calculator

## Configuring
```bash
export MONO_TOKEN='Monobank API token'
export INCOME_CURRENCY='USD'
export REPORT_CURRENCY='UAH'
export REPORT_YEAR='2021'
# if needed
export REPORT_QUARTER='4'
```

## Run app
```bash
make install-requirements
python3.9 src/main.py
```
