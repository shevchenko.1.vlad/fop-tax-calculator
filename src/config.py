from os import getenv


class AppConfig:
    mono_token = getenv("MONO_TOKEN")
    income_currency = getenv("INCOME_CURRENCY", "USD")
    report_currency = getenv("REPORT_CURRENCY", "UAH")
    report_year = int(getenv("REPORT_YEAR", 2021))
    report_quarter = int(getenv("REPORT_QUARTER")) if getenv("REPORT_QUARTER") else None
