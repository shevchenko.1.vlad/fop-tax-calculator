import calendar
from datetime import date, datetime
from typing import Union


def date_to_datetime(d: date) -> datetime:
    return datetime(year=d.year, month=d.month, day=d.day)


def datetime_quarters_range(year: int, quarter_range: tuple) -> list[tuple[datetime]]:
    for month in quarter_range:
        _, last_day = calendar.monthrange(year, month)
        from_date = datetime(year=year, month=month, day=1)
        to_date = datetime(year=year, month=month, day=last_day)
        yield from_date, to_date


def get_months_of_quarters(quarter: Union[int, None]) -> tuple[tuple[int, int, int]]:
    quarters = (
        (1, 2, 3),
        (4, 5, 6),
        (7, 8, 9),
        (10, 11, 12),
    )
    return (quarters[quarter - 1],) if quarter else quarters
