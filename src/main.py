import logging

from monobank import Client

from src.config import AppConfig
from src.dateutils import get_months_of_quarters
from src.monobank_api import get_mono_fops, get_statements, print_statements
from src.nbu_api import get_nbu_rate

logging.basicConfig(filename="info.log", level=logging.INFO)

if __name__ == "__main__":
    mono_client = Client(AppConfig.mono_token)
    fops = get_mono_fops(client=mono_client)
    report_period = get_months_of_quarters(quarter=AppConfig.report_quarter)
    income_currency = AppConfig.income_currency
    report_currency = AppConfig.report_currency

    for fop in fops:
        print(f"Entrepreneur account for {fop.currency.name}")
        print(
            f"Report period: {AppConfig.report_year} year"
            f'{f", {AppConfig.report_quarter} quarter" if AppConfig.report_quarter else ""}.'
        )

        mono_year_sum = 0
        nbu_year_sum = 0
        for num, quarter_months in enumerate(report_period, 1):
            quarter_number = AppConfig.report_quarter if AppConfig.report_quarter else num
            stmts = get_statements(mono_client, account_id=fop.id, year=AppConfig.report_year, quarter=quarter_months)
            swift_stmts = [stmt for stmt in stmts if stmt.is_swift]
            print(f"Quarter {quarter_number}: \n")
            print_statements(stmts, fop.currency.alpha_3)

            mono_quarter_sum = sum(st.amount for st in stmts if st.amount > 0)
            mono_year_sum += mono_quarter_sum
            print(f"\nMonobank quarter sum: {mono_quarter_sum:.2f} {income_currency}")

            nbu_quarter_sum = sum(st.amount * get_nbu_rate(st.time, income_currency) for st in stmts if st.amount > 0)
            nbu_year_sum += nbu_quarter_sum
            print(f"\nNBU quarter sum: {nbu_quarter_sum:.2f} {report_currency}")

        if not AppConfig.report_quarter:
            print(f"Monobank year sum: {mono_year_sum:.2f} {income_currency}", end="\n\n")
            print(f"NBU year sum: {nbu_year_sum:.2f} {report_currency}", end="\n\n")
