from datetime import datetime
from typing import Any, Optional

from pycountry import currencies
from pydantic import BaseModel


class Account(BaseModel):
    id: str
    send_id: str
    balance: Optional[int]
    credit_limit: Optional[int]
    type: str
    currency: Optional[Any]
    cashback_type: Optional[str]
    masked_pan: list
    iban: str

    @classmethod
    def make_account(cls, entity: dict):
        currency = entity.get("currencyCode")
        balance = entity.get("balance")
        credit_limit = entity.get("creditLimit")

        if currency:
            currency = currencies.get(numeric=str(currency))
        if balance:
            balance = balance / 100.0
        if credit_limit:
            credit_limit = credit_limit / 100.0

        return cls(
            id=entity.get("id"),
            send_id=entity.get("sendId"),
            balance=balance,
            credit_limit=credit_limit,
            type=entity.get("type"),
            currency=currency,
            cashback_type=entity.get("cashbackType"),
            masked_pan=entity.get("maskedPan"),
            iban=entity.get("iban"),
        )


class Statement(BaseModel):
    id: str
    time: Optional[datetime]
    description: str
    mcc: int
    original_mcc: Optional[int]
    hold: bool
    amount: Optional[int]
    operation_amount: int
    currency: Optional[Any]
    commission_rate: int
    cashback_amount: int
    balance: int
    comment: Optional[str]
    receipt_id: Optional[str]
    invoice_id: Optional[str]
    counter_edrpou: Optional[str]
    counter_iban: Optional[str]
    is_swift: bool

    @classmethod
    def make_statement(cls, entity: dict):
        amount = entity.get("amount")
        time = entity.get("time")
        currency = entity.get("currencyCode")
        counter_edrpou = entity.get("counterEdrpou")
        counter_iban = entity.get("counterEdrpou")
        receipt_id = entity.get("counterEdrpou")
        comment = entity.get("counterEdrpou")

        if amount:
            amount = amount / 100.0
        if time:
            time = datetime.fromtimestamp(time)
        if currency:
            currency = currencies.get(numeric=str(currency))
        if counter_edrpou:
            counter_edrpou = ""
        if counter_iban:
            counter_iban = ""
        if receipt_id:
            receipt_id = ""
        if comment:
            comment = ""

        is_swift = bool("swift" in entity.get("description", "").lower())

        return cls(
            id=entity.get("id"),
            time=time,
            description=entity.get("description"),
            mcc=entity.get("mcc"),
            original_mcc=entity.get("original_mcc"),
            hold=entity.get("hold"),
            amount=amount,
            operation_amount=entity.get("operationAmount"),
            currency=currency,
            commission_rate=entity.get("commissionRate"),
            cashback_amount=entity.get("cashbackAmount"),
            balance=entity.get("balance"),
            comment=comment,
            receipt_id=receipt_id,
            invoice_id=entity.get("invoiceId"),
            counter_edrpou=counter_edrpou,
            counter_iban=counter_iban,
            is_swift=is_swift,
        )
