import logging

from monobank import Client
from tabulate import tabulate

from src.config import AppConfig
from src.dateutils import datetime_quarters_range
from src.models import Account, Statement
from src.utils import send_request


def get_statements(client: Client, account_id: str, year: int, quarter: tuple) -> list[Statement]:
    statements: list[Statement] = []
    for first_date, last_date in datetime_quarters_range(year, quarter):
        api_statements = send_request(lambda: client.get_statements(account_id, first_date, last_date))
        statements += [Statement.make_statement(api_statement) for api_statement in api_statements]
        logging.info(f"Done for {first_date} - {last_date}")

    return sorted(statements, key=lambda s: s.time)


def print_statements(statements: list[Statement], currency: str) -> None:
    res_statements = [
        (
            f"{statement.amount:.2f}",
            statement.time.strftime("%Y-%m-%d %H:%M:%S"),
            currency,
            statement.description,
        )
        for statement in statements
        if statement.amount > 0
    ]
    print(tabulate(res_statements, headers=["Amount", "Time", "Currency", "Description"]))


def get_mono_fops(client: Client) -> list[Account]:
    api_accounts = send_request(client.get_client_info)["accounts"]
    accounts = [Account.make_account(api_account) for api_account in api_accounts]
    return list(
        filter(
            lambda account: account.type == "fop" and account.currency.alpha_3 == AppConfig.income_currency, accounts
        )
    )
