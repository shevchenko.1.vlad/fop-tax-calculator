from datetime import datetime

import requests


def get_nbu_rate(date: datetime, currency: str) -> int:
    nbu_date = date.strftime("%Y%m%d")
    nbu_url = f"https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode={currency}&date={nbu_date}&json"
    res = requests.get(nbu_url)
    return res.json()[0]["rate"]
