from time import sleep
from typing import Any, Callable

from monobank import TooManyRequests


def send_request(request: Callable) -> Any:
    while True:
        try:
            return request()
        except TooManyRequests:
            sleep(1)
        else:
            break
